import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

test('renders learn react link', () => {
  console.log('test written by zahoor');
  render(<App />);
  const linkElement = screen.getByText(/Zahoor Ahmed Naqeebi is learing gitlab/i);
  expect(linkElement).toBeInTheDocument();
});
